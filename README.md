# TTodo

## 介绍

一个记录任务，规划时间的微信小程序。

![](https://img.90play.cn/gHI0FeQc4QpfyTyR)

## 预览

![](https://img.90play.cn/3prYQ7ru0PKcHr1V?imageMogr2/thumbnail/640x/blur/1x0/quality/100)

![](https://img.90play.cn/zK2zBjSVGAZ2j3Il?imageMogr2/thumbnail/640x/blur/1x0/quality/100)

![](https://img.90play.cn/78IJgnnYEZDbIp1Y?imageMogr2/thumbnail/640x/blur/1x0/quality/100)

![](https://img.90play.cn/eScAUuJNCmaEL89w?imageMogr2/thumbnail/640x/blur/1x0/quality/100)

## 功能

- 添加任务，设置任务截止时间、分类、紧急状况、是否为日常、任务名、任务描述
- 标记为已完成
- 删除任务
- 统计已完成任务数
- 展示未完成任务

## 部署

1. clone 到本地
> git clone git@github.com:JoeC95/TTodo.git
2. 从微信小程序开发ide 中打开此文件夹即可

## ToDo

- [ ] 增加搜索任务功能
- [ ] 增加任务过滤排序功能
- [ ] 为每个任务生成单独的id
- [ ] 同步任务到后端服务器
- [ ] 个人信息页面
- [ ] 设置界面

## 相关资源

[官方文档](https://mp.weixin.qq.com/debug/wxadoc/dev/index.html)

## 遇到的坑

我是一个后端程序员，因为比较好奇小程序，所以做一个玩玩。对前端不怎么熟悉，因此遇到一些坑，记录下来。

#### 页面布局之 flex

#### app.json　配置标题（navigationBarTitleText）

#### 数据绑定

#### 跳转页面

#### 导入　js 模块

#### 设置数据(setData)

#### linux 环境下开发

未完

## LICENSE

GPL
